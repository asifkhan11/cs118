import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Ex12
{
	public void controlRobot(IRobot robot)
	{
		int randno;
		int direction;
		do
		{
			randno = (int) Math.round(Math.random()*3);	//Generating random number to choose a direction randomly
			if (randno == 0)
	    			direction = IRobot.LEFT;
			else if (randno == 1)
	    			direction = IRobot.RIGHT;
			else if (randno == 2)
	    			direction = IRobot.BEHIND;
			else 
		    		direction = IRobot.AHEAD;
     		}while(robot.look(direction)==IRobot.WALL);	//keeps choosing a direction till it finds a non wall one to avoid collision
	
		robot.face(direction);  // Face the robot in this direction
		robot.advance();        // and move the robot
		System.out.println("Target at: "+isTargetNorth(robot));//To check the output of isTargetNorth()
	}
	/**
	*Function: isTargetNorth()
	*@param: robot
	*returns 1 if the target is at the North of the robot
	*returns 0 if they are on the same lattitude 
	*returns -1 if the target is at the South of the robot
	*/
	private byte isTargetNorth(IRobot robot)
    	{
    		int robotY=robot.getLocationY();	//To obtain Y value of robot
    		int targetY=robot.getTargetLocation().y;	//To obtain Y value of target
    		if(robotY==targetY)
    			return 0;
    		else if(robotY>targetY)
	    		return 1;
    		else
    			return -1;
    	}
}