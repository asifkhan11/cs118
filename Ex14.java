import uk.ac.warwick.dcs.maze.logic.IRobot;
public class Ex14
{
	public void controlRobot(IRobot robot)
	{
		int direction;
		direction = getAvailableDirection(robot);
		robot.face(direction);  //Face the robot in this direction   
		robot.advance();        // and move the robot 
	}
	/**
	*Function: isTargetNorth()
	*@param: robot
	*returns 1 if the target is at the North of the robot
	*returns 0 if they are on the same lattitude 
	*returns -1 if the target is at the South of the robot
	*/
	private byte isTargetNorth(IRobot robot)
    	{
    		int robotY=robot.getLocationY();	//To obtain Y value of robot
    		int targetY=robot.getTargetLocation().y;	//To obtain Y value of target
    		if(robotY==targetY)
    			return 0;
    		else if(robotY>targetY)
	    		return 1;
    		else
    			return -1;
    	}
    /*	public void reset()
	{
		ControlTest.printResults();
	}*/
	/**
	*Function: isTargetEast()
	*@param: robot
	*returns 1 if the target is at the East of the robot
	*returns 0 if they are on the same lattitude 
	*returns -1 if the target is at the West of the robot
	*/
	private byte isTargetEast(IRobot robot)
    	{
    		int robotX=robot.getLocationX();	//To obtain X value of robot
    		int targetX=robot.getTargetLocation().x;	//To obtain X value of target
    		if(robotX==targetX)
    			return 0;
    		else if(robotX<targetX)
	    		return 1;
    		else
    			return -1;
    	}
    	/**
	*Function: getAvailableDirection()
	*@param: robot
	*returns a randomly chosen direction which does not leads to the collision
	*/
    	private int getAvailableDirection(IRobot robot)
        	{
	            int randno;
	            int direction;
	            do
		{
			randno = (int) Math.round(Math.random()*3);	//Generating random number to choose a direction randomly
			if (randno == 0)
	    			direction = IRobot.LEFT;
			else if (randno == 1)
	    			direction = IRobot.RIGHT;
			else if (randno == 2)
	    			direction = IRobot.BEHIND;
			else 
		    		direction = IRobot.AHEAD;
     		}while(robot.look(direction)==IRobot.WALL);	//keeps choosing a direction till it finds a non wall one to avoid collision
        		return direction;
        	}
        	/**
	*Function: northController()
	*@param: robot
	*returns optimal direction to reach to the target
	*if optimal s not possible, returns any randomly chosen direction which does not leads to the collision
	*/
            	private int northController(IRobot robot)
            	{
            		int direction=0;
            		byte isNorth=isTargetNorth(robot);
            		if(isNorth==1)
            			direction = IRobot.AHEAD;
            		else if(isNorth==-1)
            			direction=IRobot.BEHIND;
            		if(direction==0 || robot.look(direction)==IRobot.WALL)
            		{
            			byte isEast=isTargetEast(robot);
            			if(isEast==1)
            				direction=IRobot.RIGHT;
            			else if(isEast==-1)
            				direction=IRobot.LEFT;
            		}
            		if(robot.look(direction)==IRobot.WALL)
            			return getAvailableDirection(robot);
            		return direction;
            	}
}