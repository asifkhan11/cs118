import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Ex23
{
	private int pollRun=0;
	private RobotData robotData;
	private int explorerMode;
	public void controlRobot(IRobot robot)
	{
		int direction;
		if((robot.getRuns()==0) && (pollRun==0))
		{
			robotData=new RobotData();
			explorerMode=1;//setting explore mode
		}
		pollRun++;
		if(explorerMode==1)
			direction=exploreControl(robot);
		else
			direction=backtrackControl(robot);		
		robot.face(direction);
		robot.advance();
	}
	/**
	*Function: exploreControl()
	*@param: robot
	*returns an unexplored direction 
	*/
	private int exploreControl(IRobot robot)
	{
		int exits=nonwallExits(robot);
		int direction=0;
		if(exits==beenbeforeExits(robot))
		{
			explorerMode=0;
			return IRobot.BEHIND;
		}
		if(exits==1)
		{
			direction=deadEnd(robot);
			if(pollRun>0)
				explorerMode=0;//setting backtrack mode
		}
		else if(exits==2)
			direction=corridor(robot);
		else if(exits==3)
			direction=junction(robot);
		else
			direction=crossRoad(robot);
		if(exits>2 && beenbeforeExits(robot)<=1)
			robotData.push(robot.getHeading());
		return direction;
	}
	/**
	*Function: backtrackControl()
	*@param: robot
	*returns a direction which should be chosen while backtracking
	*/
	private int backtrackControl(IRobot robot)
	{
		int direction=0;
		int exits=nonwallExits(robot);
		if(exits==1)
			direction=deadEnd(robot);
		else if(exits==2)
			direction=corridor(robot);
		else
		{
			if(passageExits(robot)>0)
			{
				explorerMode=1;
				return exploreControl(robot);
			}
			else
			{
				robot.setHeading(oppHeading(robotData.pop()));
				if(robot.look(IRobot.AHEAD)!=IRobot.WALL)
					return IRobot.AHEAD;
				else
					return getAvailableDirection(robot);
			}
		}
		return direction;
	}
	/**
	*Function: oppHeading()
	*@param: heading
	*returns the opposite heading of given heading
	*/
	private int oppHeading(int heading)
	{
		if(heading==IRobot.SOUTH)
			return IRobot.NORTH;
		else if(heading==IRobot.NORTH)
			return IRobot.SOUTH;
		else if(heading==IRobot.EAST)
			return IRobot.WEST;
		return IRobot.EAST;
	}
	/**
	*Function: nonwallExits()
	*@param: robot
	*returns the number of non-wall blocks adjacent to the robot
	*/
	private int nonwallExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				exits++;
		}
		return exits;
	}
	/**
	*Function: passageExits()
	*@param: robot
	*returns the number of passage blocks adjacent to the robot
	*/
	private int passageExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	   		if(robot.look(i)==IRobot.PASSAGE)
				exits++;
		}
		return exits;
	}
	/**
	*Function: beenbeforeExits()
	*@param: robot
	*returns the number of beenbefore blocks adjacent to the robot
	*/
	private int beenbeforeExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)==IRobot.BEENBEFORE)
				exits++;
		}
		return exits;
	}
	/**
	*Function: isValidRandom()
	*@param: flag
	*returns 1 if all the diections are looked
	*returns 0 if there is a possibility that all the directions are not looked
	*/
	private byte isValidRandom(byte [] flag)
	{
		for(int i=0;i<flag.length;i++)
			if(flag[i]!=1)
				return 0;
		return 1;
	}
	/**
	*Function: choosePassage()
	*@param: robot
	*returns direction which is not explored
	*/
	private int choosePassage(IRobot robot)
	{
		int rno=0;
		byte[] flag=new byte[4];
		do
	    	{
	    		rno = (int) (Math.random()*4);
	    		if(robot.look(IRobot.AHEAD+rno)==IRobot.PASSAGE)
				break;
			flag[rno]=1;
		}while(isValidRandom(flag)==0);
		return IRobot.AHEAD+rno;
	}
	/**
	*Function: deadEnd()
	*@param: robot
	*returns a direction which does not cause a collision
	*/
	private int deadEnd(IRobot robot)
	{
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				return i;
		}
		return 0;
	}
	/**
	*Function: corridor()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int corridor(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: junction()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int junction(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: crossRoad()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int crossRoad(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: getAvailableDirection()
	*@param: robot
	*returns any randomly chosen direction which does not cause a collision
	*and if not possible returns a random direction
	*/
	private int getAvailableDirection(IRobot robot)
        	{
            		int randno;
	            	int direction;
	            do
            		{
		              randno = (int) (Math.random()*4);
                		if (randno == 0)
                    			direction = IRobot.LEFT;
            			else if (randno == 1)
                    			direction = IRobot.RIGHT;
                		else if (randno == 2)
                    			direction = IRobot.BEHIND;
                		else 
                    			direction = IRobot.AHEAD;
            		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one
            
            		return direction;
            	}
	/**
	*Function: reset()
	*This method is called when reset button is clicked
	*/
	public void reset()
	{
		robotData.resetJunctionCounter();
		explorerMode=1;
	}
}
class RobotData
{
	private static int maxJunctions=10000;
	private static int junctionCounter;
	private int[] junctions;
	/**
	*Constructor to initialize RobotData
	*/
	public RobotData()
	{
		junctionCounter=0;
		junctions=new int[maxJunctions];
	}
	/**
	*Function: resetJunctionCounter()
	*To reset junctionCounter
	*Should called to reset the counter for current run
	*/
	public void resetJunctionCounter()
	{
		junctionCounter=0;
	}
	/**
	*Function: recordJunction()
	*Records unexplored junctions details
	*/
	public void push(int heading)
	{
		junctions[junctionCounter++]=heading;
	}
	public int pop()
	{
		return junctions[--junctionCounter];
	}
}