import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Ex6
{
	int l,r,f,b;	//To store the count of left, right, ahead, behind moves

	public void controlRobot(IRobot robot)
	{
		int randno;
		int direction;
		do
		{
			randno = (int) Math.round(Math.random()*3);	//Generating random number to choose a direction randomly
			if (randno == 0)
				direction = IRobot.LEFT;
			else if (randno == 1)
	    			direction = IRobot.RIGHT;
			else if (randno == 2)
	    			direction = IRobot.BEHIND;
			else 
	    			direction = IRobot.AHEAD;
		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one to avoid collision
		//to check whether robot is at crossroad, junction, corridor or deadend
	    	int nonwallexits=0;	//To store the count of non-wall blocks adjacent to the robot	
	    	for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)	//To loop through all four adjacent blocks and look for non-wall exits
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				nonwallexits++;
		}
		String s="";	//To store current situation of the robot, that is whether at a deadend, corridor, junction, crossroad 
		if(nonwallexits==4)
			s=" at a crossroad";
		else if(nonwallexits==3)
			s=" at a junction";
		else if(nonwallexits==2)
			s=" down a corridor";
		else
			s=" at a deadend";
		//To print the log and summary of moves
		if(direction==IRobot.LEFT)
		{
			System.out.println("I'm going left"+s);
			System.out.println("Summary of moves: Forward="+f+" Left="+(++l)+" Right="+r+" Backwards="+b);
	 	}
		else if(direction==IRobot.RIGHT)
		{
			System.out.println("I'm going right"+s);
			System.out.println("Summary of moves: Forward="+f+" Left="+l+" Right="+(++r)+" Backwards="+b);
		}
		else if(direction==IRobot.BEHIND)
		{
			System.out.println("I'm going backwards"+s);
			System.out.println("Summary of moves: Forward="+f+" Left="+l+" Right="+r+" Backwards="+(++b));
		}
		else
		{
			System.out.println("I'm going forward"+s);
			System.out.println("Summary of moves: Forward="+(++f)+" Left="+l+" Right="+r+" Backwards="+b);
		}
		robot.face(direction);  // Face the robot in this direction
		robot.advance();        // and move the robot
	}
}