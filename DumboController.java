import uk.ac.warwick.dcs.maze.logic.IRobot;

public class DumboController
{
    public void controlRobot(IRobot robot)
    {
        int direction,heading;
        heading=robot.getHeading();
        if(heading==IRobot.NORTH)
            direction=northController(robot);
        else if(heading==IRobot.SOUTH)
            direction=southController(robot);
        else if(heading==IRobot.EAST)
            direction=eastController(robot);
        else
            direction=westController(robot);

        robot.face(direction);//Face the robot in this direction
        robot.advance();//and move the robot
    }
    /**
    *Function: isTargetNorth()
    *@param: robot
    *returns 1 if the target is at the North of the robot
    *returns 0 if they are on the same lattitude 
    *returns -1 if the target is at the South of the robot
    */
    private byte isTargetNorth(IRobot robot)
        {
            int robotY=robot.getLocationY();    //To obtain Y value of robot
            int targetY=robot.getTargetLocation().y;    //To obtain Y value of target
            if(robotY==targetY)
                return 0;
            else if(robotY>targetY)
                return 1;
            else
                return -1;
        }
    /**
    *Function: isTargetEast()
    *@param: robot
    *returns 1 if the target is at the East of the robot
    *returns 0 if they are on the same lattitude 
    *returns -1 if the target is at the West of the robot
    */
    private byte isTargetEast(IRobot robot)
        {
            int robotX=robot.getLocationX();    //To obtain X value of robot
            int targetX=robot.getTargetLocation().x;    //To obtain X value of target
            if(robotX==targetX)
                return 0;
            else if(robotX<targetX)
                return 1;
            else
                return -1;
        }
        /**
    *Function: getAvailableDirection()
    *@param: robot
    *returns a randomly chosen direction which does not leads to the collision
    */
        private int getAvailableDirection(IRobot robot)
            {
                int randno;
                int direction;
                do
        {
            randno = (int) Math.round(Math.random()*3); //Generating random number to choose a direction randomly
            if (randno == 0)
                    direction = IRobot.LEFT;
            else if (randno == 1)
                    direction = IRobot.RIGHT;
            else if (randno == 2)
                    direction = IRobot.BEHIND;
            else 
                    direction = IRobot.AHEAD;
            }while(robot.look(direction)==IRobot.WALL); //keeps choosing a direction till it finds a non wall one to avoid collision
                return direction;
            }
            /**
    *Function: controller()
    *@param: robot, conditional expression dirOne, conditional expression dirOneOpp, conditional expression dirTwo, conditional expression dirTwoOpp
    *a generic controller to be called by northController, southController, eastController, westController with appropriate conditional expressions
    *returns optimal direction to reach to the target
    *if optimal s not possible, returns any randomly chosen direction which does not leads to the collision
    */
            private int controller(IRobot robot,boolean dirOne,boolean dirOneOpp,boolean dirTwo, boolean dirTwoOpp)
            {
                int direction=0;
                           if(dirOne)
                                        direction = IRobot.AHEAD;
                            else if(dirOneOpp)
                                        direction=IRobot.BEHIND;
                            if(direction==0 || robot.look(direction)==IRobot.WALL)
                            {
                                        if(dirTwo)
                                                direction=IRobot.RIGHT;
                                        else if(dirTwoOpp)
                                                direction=IRobot.LEFT;
                            }
                            if(robot.look(direction)==IRobot.WALL)
                                        return getAvailableDirection(robot);
                            return direction;
            }
            private int northController(IRobot robot)
            {
                            byte isNorth=isTargetNorth(robot);
                            byte isEast=isTargetEast(robot);
                            return controller(robot,isNorth==1,isNorth==(-1),isEast==1,isEast==(-1));
             }
            private int southController(IRobot robot)
            {   
                            byte isNorth=isTargetNorth(robot);
                            byte isEast=isTargetEast(robot);
                            return controller(robot,isNorth==(-1),isNorth==1,isEast==(-1),isEast==1);
            }
            private int eastController(IRobot robot)
            {
                            byte isNorth=isTargetNorth(robot);
                            byte isEast=isTargetEast(robot);
                            return controller(robot,isEast==1,isEast==(-1),isNorth==(-1),isNorth==1);
            }
            private int westController(IRobot robot)
            {
                            byte isNorth=isTargetNorth(robot);
                            byte isEast=isTargetEast(robot);
                            return controller(robot,isEast==(-1),isEast==1,isNorth==1,isNorth==(-1));
            }
}