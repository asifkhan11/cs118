import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Ex19
{
	public void controlRobot(IRobot robot)
	{
		int direction,exits;
		exits=nonwallExits(robot);
		if(exits==1)
			direction=deadEnd(robot);
		else if(exits==2)
			direction=corridor(robot);
		else if(exits==3)
			direction=junction(robot);
		else
			direction=crossRoad(robot);
		robot.face(direction);
		robot.advance();
	}
	/**
	*Function: nonwallExits()
	*@param: robot
	*returns the number of non-wall blocks adjacent to the robot
	*/
	private int nonwallExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				exits++;
		}
		return exits;
	}
	/**
	*Function: passageExits()
	*@param: robot
	*returns the number of passage blocks adjacent to the robot
	*/
	private int passageExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)==IRobot.PASSAGE)
				exits++;
		}
		return exits;
	}
	/**
	*Function: isValidRandom()
	*@param: flag
	*returns 1 if all the diections are looked
	*returns 0 if there is a possibility that all the directions are not looked
	*/
	private byte isValidRandom(byte [] flag)
	{
		for(byte i=0;i<flag.length;i++)
			if(flag[i]!=1)
				return 0;
		return 1;
	}
	/**
	*Function: choosePassage()
	*@param: robot
	*returns direction which is not explored
	*/
	private int choosePassage(IRobot robot)
	{
		int rno=0;
		byte[] flag=new byte[4];
		do
	    	{
	    		rno = (int) (Math.random()*4);
	    		if(robot.look(IRobot.AHEAD+rno)==IRobot.PASSAGE)
				break;
			flag[rno]=1;
		}while(isValidRandom(flag)==0);
		return IRobot.AHEAD+rno;
	}
	/**
	*Function: deadEnd()
	*@param: robot
	*returns a direction which does not cause a collision
	*/
	private int deadEnd(IRobot robot)
	{
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				return i;
		}
		return 0;
	}
	/**
	*Function: corridor()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int corridor(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: junction()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int junction(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: crossRoad()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int crossRoad(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: getAvailableDirection()
	*@param: robot
	*returns any randomly chosen direction which does not cause a collision
	*and if not possible returns a random direction
	*/
	private int getAvailableDirection(IRobot robot)
        	{
            		int randno;
	            	int direction;
	            do
            		{
		                randno = (int) Math.round(Math.random()*3);
                		if (randno == 0)
                    			direction = IRobot.LEFT;
            			else if (randno == 1)
                    			direction = IRobot.RIGHT;
                		else if (randno == 2)
                    			direction = IRobot.BEHIND;
                		else 
                    			direction = IRobot.AHEAD;
            		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one
            
            		return direction;
        	}
}