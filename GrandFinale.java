import uk.ac.warwick.dcs.maze.logic.IRobot;

public class GrandFinale
{
	private int pollRun=0;
	private int getRuns;
	private RobotData robotData;
	private int explorerMode;
	private int initialHead,initialDirection;
	public void controlRobot(IRobot robot)
	{
		int direction=0;
		getRuns=robot.getRuns();
		if(getRuns>0)	//If this is 2nd run or later
		{
			if(pollRun==0)
			{
				robot.setHeading(initialHead);
				direction=initialDirection;
			}
			else
				direction=reachTarget(robot);
		}
		else	//if 1st run
		{
			if(pollRun==0)
			{
				robotData=new RobotData();
				explorerMode=1;//setting explore mode
				initialHead=robot.getHeading();
			}
			if(pollRun==1)	//1st move of 1st run
				initialDirection=direction;
			if(explorerMode==1)
				direction=exploreControl(robot);
			else
				direction=backtrackControl(robot);
		}
		pollRun++;
		robot.face(direction);
		robot.advance();
	}
	/**
	*Function: reachTarget()
	*@param: robot
	*returns an optimal direction which was explored in 1st run 
	*/
	private int reachTarget(IRobot robot)
	{
		int exits=nonwallExits(robot);
		int direction=0;
		if(exits==1)
			direction=deadEnd(robot);
		else if(exits==2)
			direction=corridor(robot);
		else
		{	
			int a[]=new int[2];
			a=robotData.getPath(robot.getLocationX(),robot.getLocationY());
			robot.setHeading(a[0]);
			direction=a[1];
		}
		return direction;
	}
	/**
	*Function: exploreControl()
	*@param: robot
	*returns an unexplored direction 
	*/
	private int exploreControl(IRobot robot)
	{
		int exits=nonwallExits(robot);
		int direction=0;
		if(exits==beenbeforeExits(robot))
		{
			explorerMode=0;
			return IRobot.BEHIND;
		}
		if(exits==1)
		{
			direction=deadEnd(robot);
			if(pollRun>0)
				explorerMode=0;//setting backtrack mode
		}
		else if(exits==2)
			direction=corridor(robot);
		else if(exits==3)
			direction=junction(robot);
		else
			direction=crossRoad(robot);
		if(exits>2)
		{
			robotData.recordJunction(robot.getLocationX(),robot.getLocationY(),robot.getHeading());
			robotData.insertPath(robot.getLocationX(),robot.getLocationY(),robot.getHeading(),direction);
		}
		return direction;
	}
	/**
	*Function: backtrackControl()
	*@param: robot
	*returns a direction which should be chosen while backtracking
	*/
	private int backtrackControl(IRobot robot)
	{
		int direction=0;
		int exits=nonwallExits(robot);
		if(exits==1)
			direction=deadEnd(robot);
		else if(exits==2)
			direction=corridor(robot);
		else
		{
			if(passageExits(robot)>0)
			{
				explorerMode=1;
				return exploreControl(robot);
			}
			else
			{	int head=robotData.searchJunction(robot.getLocationX(),robot.getLocationY());
				robot.setHeading(oppHeading(head));
				direction=IRobot.AHEAD;
				if(robot.look(direction)!=IRobot.WALL)
					return direction;
				else
					return getAvailableDirection(robot);
			}
		}
		return direction;
	}
	/**
	*Function: oppHeading()
	*@param: heading
	*returns the opposite heading of given heading
	*/
	private int oppHeading(int heading)
	{
		if(heading==IRobot.SOUTH)
			return IRobot.NORTH;
		else if(heading==IRobot.NORTH)
			return IRobot.SOUTH;
		else if(heading==IRobot.EAST)
			return IRobot.WEST;
		return IRobot.EAST;
	}
	/**
	*Function: nonwallExits()
	*@param: robot
	*returns the number of non-wall blocks adjacent to the robot
	*/
	private int nonwallExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				exits++;
		}
		return exits;
	}
	/**
	*Function: passageExits()
	*@param: robot
	*returns the number of passage blocks adjacent to the robot
	*/
	private int passageExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)==IRobot.PASSAGE)
				exits++;
		}
		return exits;
	}
	/**
	*Function: beenbeforeExits()
	*@param: robot
	*returns the number of beenbefore blocks adjacent to the robot
	*/
	private int beenbeforeExits(IRobot robot)
	{
		int exits=0;
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)==IRobot.BEENBEFORE)
				exits++;
		}
		return exits;
	}
	/**
	*Function: isValidRandom()
	*@param: flag
	*returns 1 if all the diections are looked
	*returns 0 if there is a possibility that all the directions are not looked
	*/
	private byte isValidRandom(byte [] flag)
	{
		for(byte i=0;i<flag.length;i++)
			if(flag[i]!=1)
				return 0;
		return 1;
	}
	/**
	*Function: choosePassage()
	*@param: robot
	*returns direction which is not explored
	*/
	private int choosePassage(IRobot robot)
	{
		int rno=0;
		byte[] flag=new byte[4];
		do
	    	{
	    		rno = (int) (Math.random()*4);
	    		if(robot.look(IRobot.AHEAD+rno)==IRobot.PASSAGE)
				break;
			flag[rno]=1;
		}while(isValidRandom(flag)==0);
		return IRobot.AHEAD+rno;
	}
	/**
	*Function: deadEnd()
	*@param: robot
	*returns a direction which does not cause a collision
	*/
	private int deadEnd(IRobot robot)
	{
		for(int i=IRobot.AHEAD;i<=IRobot.LEFT;i++)
	    	{
	    		if(robot.look(i)!=IRobot.WALL)
				return i;
		}
		return 0;
	}
	/**
	*Function: corridor()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int corridor(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: junction()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int junction(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: crossRoad()
	*@param: robot
	*returns unexplored LEFT or RIGHT direction at corner, or AHEAD
	*and if not possible returns a random direction
	*/
	private int crossRoad(IRobot robot)
	{
		int direction=0;
		if(passageExits(robot)>0)
			return choosePassage(robot);
		do
		{
			direction=getAvailableDirection(robot);
		}while(direction==IRobot.BEHIND);
		return direction;
	}
	/**
	*Function: getAvailableDirection()
	*@param: robot
	*returns any randomly chosen direction which does not cause a collision
	*and if not possible returns a random direction
	*/
	private int getAvailableDirection(IRobot robot)
        	{
            		int randno;
	            	int direction;
	            do
            		{
		                randno = (int) Math.round(Math.random()*3);
                		if (randno == 0)
                    			direction = IRobot.LEFT;
            			else if (randno == 1)
                    			direction = IRobot.RIGHT;
                		else if (randno == 2)
                    			direction = IRobot.BEHIND;
                		else 
                    			direction = IRobot.AHEAD;
            		}while(robot.look(direction)==IRobot.WALL);//keeps choosing a direction till it finds a non wall one
            
            		return direction;
            	}
	/**
	*Function: reset()
	*This method is called when reset button is clicked
	*/
	public void reset()
	{
		//robotData.printJunction();
		robotData.resetJunctionCounter();
		explorerMode=1;
	}
}
class JunctionRecorder
{
	int juncX;
	int juncY;
	int arrived;
	public JunctionRecorder(int juncX,int juncY,int arrived)
	{
		this.juncX=juncX;
		this.juncY=juncY;
		this.arrived=arrived;
	}
}
class PathBuilder
{
	int arrivedHead;
	int chosenDirection;
	public PathBuilder(int heading,int direction)
	{
		this.arrivedHead=heading;
		this.chosenDirection=direction;
	}
}
class RobotData
{
	private static int maxJunctions=100000;
	private static int maxMazeSize=400;
	private static int junctionCounter;
	JunctionRecorder[] junctions;
	private PathBuilder[][] path;
	/**
	*Constructor to initialize RobotData
	*/
	public RobotData()
	{
		junctionCounter=0;
		junctions=new JunctionRecorder[maxJunctions];
		path=new PathBuilder[maxMazeSize][maxMazeSize];
	}
	/**
	*Function: resetJunctionCounter()
	*To reset junctionCounter
	*Should called to reset the counter for current run
	*/
	public void resetJunctionCounter()
	{
		junctionCounter=0;
	}
	/**
	*Function: insertPath()
	*stores optimal path details
	*/
	public void insertPath(int x,int y,int heading,int direction)
	{
		free(x,y);
		path[x][y]=new PathBuilder(heading,direction);
	}
	/**
	*Function: insertPath()
	*returns optimal path
	*/
	public int[] getPath(int x,int y)
	{
		int[] tempPath=new int[2];
		tempPath[0]=path[x][y].arrivedHead;
		tempPath[1]=path[x][y].chosenDirection;
		return tempPath;
	}
	public void recordJunction(int x,int y,int heading)
	{
		junctions[junctionCounter++]=new JunctionRecorder(x,y,heading);
	}
	public int searchJunction(int x,int y)
	{
		for(int i=0;i<junctionCounter;i++)
			if(junctions[i].juncX==x && junctions[i].juncY==y)
				return junctions[i].arrived;
		free(x,y);
		return 0;
	}
	private void free(int x,int y)
	{
		path[x][y]=null;
	}
}